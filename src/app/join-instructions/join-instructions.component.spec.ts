import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JoinInstructionsComponent } from './join-instructions.component';

describe('JoinInstructionsComponent', () => {
  let component: JoinInstructionsComponent;
  let fixture: ComponentFixture<JoinInstructionsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JoinInstructionsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(JoinInstructionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
