import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { GameService } from '../game.service';
import { Message } from '../message';
import { Ship } from '../ship';
import { WebSocketService } from '../web-socket.service';
import md5 from 'js-md5';
import { ChatComponent } from '../chat/chat.component';
import { BaseInstructionsComponent } from '../base-instructions/base-instructions.component';
@Component({
  selector: 'app-base-game',
  templateUrl: './base-game.component.html',
  styleUrls: ['./base-game.component.css']
})
export class BaseGameComponent implements OnInit, OnDestroy {
  @ViewChild('chat') chat!: ChatComponent;
  @ViewChild('instructions') instructions!: BaseInstructionsComponent;
  availableShips: Set<Ship> = this.game.allShips
  publicKey: string = "";
  connectionState: boolean = false;
  waitingFor: string = "";
  emojiTable: Array<string> = ['0️⃣', '1️⃣', '2️⃣', '3️⃣', '4️⃣', '5️⃣', '6️⃣', '7️⃣', '8️⃣', '9️⃣', '🔟']
  selectedShip: number = NaN;
  errorMessage: string = "";
  messages: Array<Message> = [];
  constructor(protected webSocket: WebSocketService, protected game: GameService) {
  
  }
  ngOnInit(): void {
    this.clearMap()
  }
  ngOnDestroy(): void {
    this.webSocket.close()
  }
  oldMsg: Array<string | number | Array<number>> = [""];
  stateShow: string = "Connecting to the web socket";
  commited: Boolean = false;

  handleMsg(msg: Array<string | number | Array<number>>) {
    if (msg[0] == "WAITING_ON") {
      this.publicKey = msg[1].toString();
      this.stateShow = "Waiting for a 🍐 on"
    }
    if (msg[0]=="MESSAGE"){
      let message = {
        sender: "opponent",
        text: msg[1].toString(),
        time: new Date()
      }
      this.messages.push(message)
    }
    if (msg[0] == "WAITING_FOR") {
      this.waitingFor = msg[1].toString()
      this.stateShow = "Please set your ships"
      console.log(this.availableShips)
      console.log(this.game.allShips)
    }

    if (msg[0] == "UPDATE")
      if (msg[1] == "PEAR_JOINED") {
        this.connectionState = true
        this.webSocket.sendMessage(JSON.stringify(["WAITING_FOR", "SET_SHIPS"]))
        this.stateShow = "A 🍐 has joined"
      }
    if (msg[0] == "SUBMIT_HASH") {
      this.game.opponentHash = msg[1].toString()
    }
    if (msg[0] == "MISS") {
      this.game.misses.push(this.game.lastShot)
      let audio = new Audio('../../assets/miss.mp3');
      audio.volume = 0.25
      audio.play();
    }
    if (msg[0] == "ERROR") {
      this.errorMessage = msg[1].toString()
      setInterval(() => { this.errorMessage = "" }, 5000)
    }
    if (msg[0] == "HIT") {
      let audio = new Audio('../../assets/bomb.mp3');
      audio.volume = 0.25
      audio.play();
      this.game.hits.push(this.game.lastShot)
      console.log(this.game.hits)
    }
    if (msg[0] == "SHIP_DESTROYED") {
      this.game.hits.push(this.game.lastShot)
      let audio = new Audio('../../assets/ship_destroyed.mp3');
      audio.volume = 0.75
      audio.play();
    }
    if (msg[0] == "ALL_SHIPS_DESTROYED") {
      this.game.hits.push(this.game.lastShot)
      this.game.result = true
      this.game.turn = 2
      let audio = new Audio('../../assets/victory.mp3');
      audio.volume = 0.25
      audio.play();

    }
    if (msg[0] == "STATE") {
      let opponentState: any
      if (Array.isArray(msg[1]))
        opponentState = msg[1]
      let validHash = (this.game.opponentHash == md5(JSON.stringify(opponentState)))
      let hitsValid = 1
      for (const [index, hit] of this.game.hits.entries()) {
        if (opponentState[hit[0]][hit[1]] == -1)
          hitsValid *= 0
        else
          hitsValid *= 1
      }
      let missesValid = 1
      for (const [index, miss] of this.game.misses.entries()) {
        if (opponentState[miss[0]][miss[1]] != -1)
          missesValid *= 0
        else
          missesValid *= 1
      }
      if (validHash && Boolean(hitsValid) && Boolean(missesValid)) {
        console.log("Honest Opponent")
      } else {
        console.log("Cheating Opponent")
      }
    }
    if (msg[0] == "BOMB" && this.game.turn == 0) {
      let shot: any = JSON.parse(msg[1].toString())
      let result = this.game.checkSquare(shot[0], shot[1])
      this.game.bomb(shot[0], shot[1])
      let cell = document.getElementById("cell-" + shot[0] + "-" + shot[1])
      let hit = "<span class='z-10'>💥</span>"
      let miss = "<span class='z-10'>✖️</span>"
      if (result != -1) {
        if (this.game.isDestroyed(result)) {
          if (this.game.isAllShipsDestroyed()) {
            this.webSocket.sendMessage('["ALL_SHIPS_DESTROYED"]')
            this.game.result = false
            this.game.turn = 2
            if (cell)
              cell.innerHTML += hit
          }
          else {
            this.webSocket.sendMessage('["SHIP_DESTROYED", "' + result + '"]')
            this.game.turn = 1
            if (cell)
              cell.innerHTML += hit
          }
        } else {
          this.webSocket.sendMessage('["HIT"]')
          let audio = new Audio('../../assets/bomb.mp3');
          audio.volume = 0.25
          audio.play();
          this.game.turn = 1
          if (cell)
            cell.innerHTML += hit
        }
      } else {
        this.webSocket.sendMessage('["MISS"]')
        let audio = new Audio('../../assets/miss.mp3');
        audio.volume = 0.25
        audio.play();
        this.game.turn = 1
        if (cell)
          cell.innerHTML += miss
      }


    }
  }

  ngDoCheck() {
    if (typeof (this.webSocket.msg) != "undefined") {
      if (this.oldMsg != this.webSocket.msg)
        this.handleMsg(this.webSocket.msg)
      this.oldMsg = this.webSocket.msg
    }
    if (this.game.turn == 2) {
      this.webSocket.sendMessage(JSON.stringify(["STATE", JSON.stringify(this.game.stateArray)]))
      this.game.turn = 3
    }
  }


  selectShip(id: number) {
    let body = document.getElementsByTagName("body")[0];
    body.classList.add("cursor-cell")
    this.selectedShip = id
  }

  placeShip(i: number, j: number) {
    if (this.stateShow == 'Please set your ships' && !Number.isNaN(this.selectedShip)) {
      let ship = Array.from(this.availableShips.values()).filter((element) => element["id"] == this.selectedShip)[0]
      this.availableShips.delete(ship)
      this.selectedShip = NaN;
      let shipSquares = [];
      ship.o = [i, j]

      for (let x = ship.o[0]; x < ship.o[0] + ship.n; x++) {
        for (let y = ship.o[1]; y < ship.o[1] + ship.m; y++) {
          this.game.stateArray[x][y] = ship.id.toString()
        }
      }

      let body = document.getElementsByTagName("body")[0];
      body.classList.remove("cursor-cell")
      this.game.ships.add(ship)
    }

  }

  clearMap() {
    this.game.ships = new Set()
    this.game.refresh()
    this.game = new GameService()
    this.availableShips = this.game.allShips

  }
  commitMap() {
    let hash = this.game.hash()
    this.webSocket.sendMessage(JSON.stringify(["SUBMIT_HASH", hash]))
    this.commited = true
    this.game.refresh()
  }
  bomb(i: number, j: number) {
    if (this.game.turn == 1) {

      this.webSocket.sendMessage(JSON.stringify(["BOMB", JSON.stringify([i, j])]))
      this.game.lastShot = [i, j]
      this.game.turn = 0

    }

  }
  showChat() {
    document.getElementById("chat-root")!.scrollIntoView();
    this.chat.show()
  }
  showInstructions() {
    document.getElementById("instructions-root")!.scrollIntoView();
    this.instructions.show()
  }
  sendMessage($event: string) {
    let message = {
      "sender": "you",
      "text": $event,
      "time": new Date()
    }

    this.messages.push(message)
    this.webSocket.sendMessage(JSON.stringify(["MESSAGE", $event]))
  }
}
