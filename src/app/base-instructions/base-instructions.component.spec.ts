import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BaseInstructionsComponent } from './base-instructions.component';

describe('BaseInstructionsComponent', () => {
  let component: BaseInstructionsComponent;
  let fixture: ComponentFixture<BaseInstructionsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BaseInstructionsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BaseInstructionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
