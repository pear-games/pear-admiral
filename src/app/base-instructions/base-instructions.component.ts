import { Component } from '@angular/core';

@Component({
  selector: 'app-base-instructions',
  templateUrl: './base-instructions.component.html',
  styleUrls: ['./base-instructions.component.css']
})
export class BaseInstructionsComponent {
  hide() {
    let root = document.getElementById("instructions-root")
    root?.classList.remove("animate__slideInRight")
    root?.classList.add("animate__slideOutRight")
    let hideButton = document.getElementById("instructions-hide-button")
    hideButton!.style.display = "none"
    root?.classList.add("hidden")
    if (window.innerWidth <= 1024) {
      document.getElementById("chat-root")!.style.display = "block"
      document.getElementById("join-game")!.style.display = "block"
    }
    window.scrollTo(0, 0);
  }
  ngOnInit(): void {
    document.getElementById("instructions-root")?.classList.add("animate__slideInRight")
  }
  show() {
    console.log("called")
    let root = document.getElementById("instructions-root")
    root?.classList.remove("animate__slideOutRight")
    root?.classList.remove("hidden")
    root?.classList.add("animate__slideInRight")
    let hideButton = document.getElementById("instructions-hide-button")
    hideButton!.style.display = "block"
    root?.scrollIntoView()
    if (window.innerWidth <= 1024) {
      document.getElementById("chat-root")!.style.display = "none"
      document.getElementById("join-game")!.style.display = "none"
    }
  }
}
