import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BgAudioButtonComponent } from './bg-audio-button.component';

describe('BgAudioButtonComponent', () => {
  let component: BgAudioButtonComponent;
  let fixture: ComponentFixture<BgAudioButtonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BgAudioButtonComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BgAudioButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
