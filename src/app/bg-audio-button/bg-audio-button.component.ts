import { Component, OnInit } from '@angular/core';
import { BgAudioServiceService } from '../bg-audio-service.service'
@Component({
  selector: 'app-bg-audio-button',
  templateUrl: './bg-audio-button.component.html',
  styleUrls: ['./bg-audio-button.component.css']
})
export class BgAudioButtonComponent implements OnInit {
  constructor(public bgAudio: BgAudioServiceService) {

  }
  ngOnInit(): void {
    let el = document.getElementById("bg-audio-btn")
    if (this.bgAudio.audio.paused) {
      el!.innerHTML = '<i class="bi bi-speaker"></i>'
    }else{
      el!.innerHTML = '<i class="bi bi-speaker-fill"></i>'
    }
  }

  stop() {
    this.bgAudio.stop()
    let el = document.getElementById("bg-audio-btn")
    el!.innerHTML = '<i class="bi bi-speaker"></i>'
  }

  play() {
    this.bgAudio.play()
    let el = document.getElementById("bg-audio-btn")
    el!.innerHTML = '<i class="bi bi-speaker-fill"></i>'
  }

  toggle() {
    let el = document.getElementById("bg-audio-btn")
    if (el!.innerHTML == '<i class="bi bi-speaker-fill"></i>')
      this.stop()
    else
      this.play()
  }
}
