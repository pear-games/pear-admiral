export interface Ship {
    id: number;
    n: number;
    m: number;
    o?: Array<number>;
  }