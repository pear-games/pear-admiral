import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { BaseInstructionsComponent } from '../base-instructions/base-instructions.component';

@Component({
  selector: 'app-new-instructions',
  templateUrl: './new-instructions.component.html',
  styleUrls: ['./new-instructions.component.css']
})
export class NewInstructionsComponent extends BaseInstructionsComponent implements OnChanges{
  @Input() publicKey!: string;

  ngOnChanges(changes: SimpleChanges): void {
    console.log(changes)
  }
  override ngOnInit(): void {
    super.ngOnInit()
    console.log(this.publicKey)
  }
  
}
