import { Component, DoCheck, OnInit, ViewChild } from '@angular/core';
import { BaseGameComponent } from '../base-game/base-game.component';

@Component({
  selector: 'app-new-game',
  templateUrl: './new-game.component.html',
  styleUrls: ['./new-game.component.css']
})

export class NewGameComponent extends BaseGameComponent implements OnInit, DoCheck {

  override ngOnInit(): void {
    super.ngOnInit()
    this.webSocket.sendMessage(JSON.stringify(["NEW_GAME"]))
    this.game.turn=1
  }

  copyPubKey(): void {
    navigator.clipboard.writeText(this.publicKey);
    let button = document.getElementById("copy")
    if (button) {
      button.innerHTML = '<i class="bi bi-clipboard-check-fill"></i>'
      setInterval(() => button!.innerHTML = '<i class="bi bi-clipboard"></i>', 750)
    }
  }
}
