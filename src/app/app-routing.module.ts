import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InitialComponent } from './initial/initial.component';
import { JoinGameComponent } from './join-game/join-game.component';
import { NewGameComponent } from './new-game/new-game.component';

const routes: Routes = [
  { path: '', component: InitialComponent },
  { path: 'new-game', component: NewGameComponent },
  { path: 'join-game', component: JoinGameComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
