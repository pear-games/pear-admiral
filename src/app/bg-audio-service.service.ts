import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BgAudioServiceService {
  audio: HTMLAudioElement= new Audio('../assets/bg.mp3');
  constructor() { }
  play() {
    this.audio.volume=0.05
    this.audio.play();
  }

  stop() {
    this.audio.pause();
  }
  

}
