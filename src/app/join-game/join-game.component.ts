import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { BaseGameComponent } from '../base-game/base-game.component';

@Component({
  selector: 'app-join-game',
  templateUrl: './join-game.component.html',
  styleUrls: ['./join-game.component.css']
})
export class JoinGameComponent extends BaseGameComponent implements OnInit {

  gameAddress = new FormControl('');
  
  override ngOnInit(): void {
    super.ngOnInit()
    this.stateShow="Enter your Pear\'s Address"
    this.game.turn = 0
  }

  joinGame() {
    this.webSocket.sendMessage(JSON.stringify(["JOIN_GAME", this.gameAddress.value]))
    this.webSocket.sendMessage(JSON.stringify(["WAITING_FOR", "SET_SHIPS"]))
  }
}
