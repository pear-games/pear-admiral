import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.css']
})
export class ToolbarComponent {
  @Output("showChat") showChat: EventEmitter<any> = new EventEmitter();
  @Output("showInstructions") showInstructions: EventEmitter<any> = new EventEmitter();
}
