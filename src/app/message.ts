export interface Message {
    sender: string,
    text: string,
    time: Date
  }