import { Injectable, NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { InitialComponent } from './initial/initial.component';
import { NewGameComponent } from './new-game/new-game.component';
import { SimpleNgWebSocket, CONNECT_URL, LOGGER } from 'simple-ng-websocket';
import { JoinGameComponent } from './join-game/join-game.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { BgAudioButtonComponent } from './bg-audio-button/bg-audio-button.component';
import { ChatComponent } from './chat/chat.component';


import { BrowserModule, HammerGestureConfig, HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { WelcomeComponent } from './welcome/welcome.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { BaseGameComponent } from './base-game/base-game.component';

import { HammerModule } from "../../node_modules/@angular/platform-browser";
import { JoinInstructionsComponent } from './join-instructions/join-instructions.component';
import { BaseInstructionsComponent } from './base-instructions/base-instructions.component';
import { NewInstructionsComponent } from './new-instructions/new-instructions.component';

import { QRCodeModule } from 'angularx-qrcode';
@NgModule({
  declarations: [
    AppComponent,
    InitialComponent,
    NewGameComponent,
    JoinGameComponent,
    BgAudioButtonComponent,
    ChatComponent,
    WelcomeComponent,
    ToolbarComponent,
    BaseGameComponent,
    JoinInstructionsComponent,
    BaseInstructionsComponent,
    NewInstructionsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    HammerModule,
    QRCodeModule,
    ReactiveFormsModule
  ],
  providers: [{ provide: CONNECT_URL, useValue: 'ws://localhost:8080' },
              { provide: LOGGER, useValue: (level:any, message:any) => console.log(message) },
              SimpleNgWebSocket,],
  bootstrap: [AppComponent]
})
export class AppModule { }
