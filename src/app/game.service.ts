import { Injectable } from '@angular/core';
import md5 from 'js-md5';
import { Ship } from './ship'
@Injectable({
  providedIn: 'root'
})
export class GameService {
  gameField: Array<number> = [9, 9]
  bombings: Array<Array<Boolean>> = new Array(this.gameField[0]).fill(0).map(() => new Array(this.gameField[1]).fill(false));
  ships: Set<Ship> = new Set()
  stateArray: Array<Array<string>> = new Array(this.gameField[0]).fill(0).map(() => new Array(this.gameField[1]).fill("-1"));
  occupiedSquares: Array<Array<number>> = new Array(this.gameField[0]).fill(0).map(() => new Array(this.gameField[1]).fill(-1));
  hits: Array<Array<number>> = []
  misses: Array<Array<number>> = []
  allShips: Set<Ship> = new Set([{ "id": 0, "n": 1, "m": 4},{ "id": 1, "n": 3, "m": 2}, { "id": 2, "n": 1, "m": 3},{ "id": 3, "n": 1, "m": 1}])
  opponentHash: string = "";
  turn: number = -1;
  lastShot: number[] = [-1, -1];
  symbols: Array<string>=["⛴","🛳","🚢","🛥"]
  result: boolean = false;
  constructor() {
    this.allShips = new Set([{ "id": 0, "n": 1, "m": 4},{ "id": 1, "n": 3, "m": 2}, { "id": 2, "n": 1, "m": 3},{ "id": 3, "n": 1, "m": 1}])
  }

  hash() {
    return md5(JSON.stringify(this.stateArray));
  }

  refresh() {
    this.calculateOccupiedSquares()
    this.calculateStateArray()
  }

  calculateOccupiedSquares() {
    this.ships.forEach(element => {
      if (element.o)
        for (let i = element["o"][0]; i < element["o"][0] + element["n"]; i++) {
          for (let j = element["o"][1]; j < element["o"][1] + element["m"]; j++) {
            this.occupiedSquares.push([i, j, element["id"]])
          }
        }
    });
  }


  addShip(ship: Ship) {
    this.calculateOccupiedSquares()
    let shipSquares = new Array()
    if (ship.o)
      for (let i = ship["o"][0]; i < ship["o"][0] + ship["n"]; i++) {
        for (let j = ship["o"][1]; j < ship["o"][1] + ship["m"]; j++) {
          shipSquares.push([i, j])
        }
      }
    if (this.occupiedSquares.length == 0) {
      this.ships.add(ship)
      this.refresh()
      return true
    }
    else {
      let accept = 1
      for (const [index, element] of shipSquares.entries()) {
        if (this.occupiedSquares.some(item => item[0] == element[0] && item[1] == element[1]) && accept == 1) {
          console.error("Conflicting squares.(" + element + ") is occupied. Ship " + ship["id"] + " won't be added.");
          accept *= 0
        }
        else {
          accept *= 1
        }
      }
      if (accept == 1) {
        this.ships.add(ship)
      }
      this.refresh()
      return Boolean(accept)
    }
  }


  checkSquare(x: number, y: number) {

    for (let i = 0; i < this.occupiedSquares.length; i++) {
      if (this.occupiedSquares[i][0] === x && this.occupiedSquares[i][1] === y)
        return this.occupiedSquares[i][2]
    }
    return -1
  }

  calculateStateArray() {
    for (let i = 0; i < 6; i++) {
      for (let j = 0; j < 6; j++) {
        this.stateArray[i][j] = this.checkSquare(i, j).toString()
      }
    }
  }

  bomb(x: number, y: number) {
      if (this.bombings[x][y] && this.bombings[x][y]==true) {
        console.error("Already bombed")
        return false
      }
      if (x < this.gameField[0] && y < this.gameField[1]) {
        this.bombings[x][y] = true
        return true
      }
      else {
        console.error("out of bounds")
        return false
      }
  }

  isDestroyed(shipId: number) {
    let squares = this.occupiedSquares.filter(item => item[2] == shipId)
    let destroyed = 0
    squares.forEach((value, index) => {
      if (this.bombings[value[0]][value[1]] == true) {
        destroyed += 1
      }
    });
    if (destroyed == squares.length)
      return true
    else
      return false
  }

  isAllShipsDestroyed() {
    let returnedValue = 1
    for (const [index, ship] of this.ships.entries()) {
      if (this.isDestroyed(ship.id))
        returnedValue *= 1
      else
        returnedValue *= 0
    }
    return Boolean(returnedValue)
  }
  isHit(x: number, y:number): Boolean{
    for (const [index, element] of this.hits.entries()) {
      if (element[0]==x && element[1] ==y){
        return true
      }
    }
    return false
  }
  isMiss(x: number, y:number){
    for (const [index, element] of this.misses.entries()) {
      if (element[0]==x && element[1] ==y){
        return true
      }
    }
    return false
  }
  isVisited(x: number, y:number){
    return this.isHit(x, y) || this.isMiss(x, y)
  }
  getShipById(id: string) :Ship{
    let nid = Number(id)
    for (const [index, element] of this.ships.entries()) {
      if (element.id==nid){
        return element
      }
    }
    return {"id":-1, "n":-1, "m":-1}
  }
}
