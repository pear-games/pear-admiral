import { TestBed } from '@angular/core/testing';

import { BgAudioServiceService } from './bg-audio-service.service';

describe('BgAudioServiceService', () => {
  let service: BgAudioServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BgAudioServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
