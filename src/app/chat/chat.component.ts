import { Component, Directive, ElementRef, EventEmitter, Input, IterableDiffers, OnChanges, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { WebSocketService } from '../web-socket.service';
import { Message } from '../message'
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})

export class ChatComponent implements OnInit {

  @Input() messages!: Array<Message>;
  @ViewChild('chatRoot')
  chatRoot!: ElementRef;
  @Output() sendMessageEvent = new EventEmitter<string>();
  showMessages: Array<Message> = [];
  differ: any;
  @Input() hidden: Boolean = false;
  inputMessage: FormControl<any> = new FormControl('');

  constructor(protected ws: WebSocketService, differs: IterableDiffers) {
    this.differ = differs.find([]).create();
  }

  sendMessage(msg: string) {
    this.sendMessageEvent.emit(msg)
    let objDiv = document.getElementById("messages-box");
    this.inputMessage.setValue(" ")
    setInterval(() => objDiv!.scrollTop = objDiv!.scrollHeight, 100)

  }

  ngOnInit(): void {
    this.showMessages = [... this.messages]
    document.getElementById("chat-root")?.classList.add("animate__slideInLeft")
    if (this.hidden){
      let root = document.getElementById("chat-root")
      root?.classList.add("hidden")
    }
  }

  ngDoCheck() {
    if (this.differ.diff(this.messages)) {
      this.showMessages = [... this.messages]
      let objDiv = document.getElementById("messages-box");
      setInterval(() => objDiv!.scrollTop = objDiv!.scrollHeight, 100)
    }
  }

  hide() {
    let root = document.getElementById("chat-root")
    root?.classList.remove("animate__slideInLeft")
    root?.classList.add("animate__slideOutLeft")
    let hideButton = document.getElementById("hide-button")
    hideButton!.style.display = "none"
    window.scrollTo(0, 0);
  }
  show() {
    let root = document.getElementById("chat-root")
    root?.classList.remove("hidden")
   
    root?.classList.remove("animate__slideOutLeft")
    root?.classList.add("animate__slideInLeft")
    
    let hideButton = document.getElementById("hide-button")
    hideButton!.style.display = "block"
    let input = document.getElementById("input-message")
    input?.focus()
    input?.scrollIntoView()
  }


}
