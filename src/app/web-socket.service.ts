import { Injectable } from '@angular/core';
import { SimpleNgWebSocket } from 'simple-ng-websocket';
@Injectable({
	providedIn: 'root'
})
export class WebSocketService {
	msg!: Array<string | number | Array<number>>;
	constructor(private ngws: SimpleNgWebSocket) {
		this.ngws.on('message', (msg) => {
			let data = JSON.parse(msg["data"])
			this.msg=data
			console.log(data)
		});
	}

	sendMessage(msg: string): void {
		this.ngws.send(msg);
	}

	close(){
		this.ngws.close()
	}
}
