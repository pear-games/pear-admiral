# PearAdmiral: A kind-of-fun P2P game built with Holepunch
Holepunch is a new cutting-edge P2P technology for communication. This game is built to mimic a [Plato](https://www.platoapp.com/) game called Sea Battle, except this one is completely P2P. This means that I don't have to run a server for players to play.

The game is pretty straightforward:
1. Run the game (New Game)
2. Let your friend run the game (Join Game)
3. Send them the public key provided in the game
4. Set your ships (and they will set theirs) and commit
5. Guess where they put their ships and bomb them
6. The first to destroy all their opponent's ships win.

## Client-Side Anti-Cheating Mechanism
The game implements a client-side anti-cheating mechanism. When you commit your ships, the app sends a hash of your map to your peer. When the game is over, the app sends the actual map so your peer can verify hits, misses, and that it matches the initial hash you sent. This makes cheating evident after-the-fact, although it doesn't prevent it.

## Try it from source
1. Run a [websocket-pear-relay](https://gitlab.com/pear-games/websocket-pear-relay).

2. Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Build it from source
1.  Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.
2.  Copy `dist/` contents to `package.nw` in a NWJS project.
3.  `touch package.json` inside `package.nw`
4.  paste the following in package.json
```
{
  "name": "pear-admiral",
  "version": "0.0.1",
  "description": "",
  "main": "main.js",
  "author": "BTCTranslator",
  "license": "ISC"
}
```
5. `touch main.js`
6. paste the following in main.js
```
nw.Window.open('index.html', {}, function(win) {});
```
7. Run a [websocket-pear-relay](https://gitlab.com/pear-games/websocket-pear-relay).
8. Run nw(.exe) in the main NWJS folder.
